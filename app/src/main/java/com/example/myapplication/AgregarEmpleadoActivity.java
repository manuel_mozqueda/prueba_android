package com.example.myapplication;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import android.support.v7.app.AppCompatActivity;

public class AgregarEmpleadoActivity extends AppCompatActivity {
    private Button btnAgregarEmpleado, btnCancelarNuevaEmpleado;
    private EditText etNombre, etFechaNacimiento,etPuesto;
    private EmpleadosController empleadosController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_empleado);

        // Instanciar vistas
        etNombre = findViewById(R.id.etNombre);
        etFechaNacimiento = findViewById(R.id.etFechaNacimiento);
        etPuesto = findViewById(R.id.etPuesto);
        btnAgregarEmpleado = findViewById(R.id.btnAgregarEmpleado);
        btnCancelarNuevaEmpleado = findViewById(R.id.btnCancelarNuevaEmpleado);
        // Crear el controlador
        empleadosController = new EmpleadosController(AgregarEmpleadoActivity.this);

        // Agregar listener del botón de guardar
        btnAgregarEmpleado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Resetear errores a ambos
                etNombre.setError(null);
                etFechaNacimiento.setError(null);
                etPuesto.setError(null);
                String nombre = etNombre.getText().toString();
                String FechaComoCadena = etFechaNacimiento.getText().toString();
                String Puesto = etPuesto.getText().toString();
                if ("".equals(nombre)) {
                    etNombre.setError("Escribe el nombre del empleado");
                    etNombre.requestFocus();
                    return;
                }
                if ("".equals(FechaComoCadena)) {
                    etFechaNacimiento.setError("Escribe la fecha de nacimiento");
                    etFechaNacimiento.requestFocus();
                    return;
                }
                if ("".equals(Puesto)) {
                    etPuesto.setError("Escribe el puesto");
                    etPuesto.requestFocus();
                    return;
                }

                Log.d("guardar","paso validacion");
                // Ya pasó la validación
                Empleado nuevaEmpleado = new Empleado(nombre,FechaComoCadena,Puesto,10);
                long id = empleadosController.nuevompleado(nuevaEmpleado);
                if (id == -1) {
                    // De alguna manera ocurrió un error
                    Toast.makeText(AgregarEmpleadoActivity.this, "Error al guardar. Intenta de nuevo", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(AgregarEmpleadoActivity.this, "Agregado", Toast.LENGTH_SHORT).show();
                    // Terminar
                    finish();
                }
            }
        });

        // El de cancelar simplemente cierra la actividad
        btnCancelarNuevaEmpleado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
