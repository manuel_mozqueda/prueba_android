package com.example.myapplication;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import android.support.v7.app.AppCompatActivity;

public class EditarEmpleadoActivity extends AppCompatActivity {
    private EditText etEditarNombre, etEditarFechaNacimiento,etEditarPuesto;
    private Button btnGuardarCambios, btnCancelarEdicion;
    private Empleado empleado;//La empleado que vamos a estar editando
    private EmpleadosController empleadosController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_empleado);

        // Recuperar datos que enviaron
        Bundle extras = getIntent().getExtras();
        // Si no hay datos (cosa rara) salimos
        if (extras == null) {
            finish();
            return;
        }
        // Instanciar el controlador de las empleados
        empleadosController = new EmpleadosController(EditarEmpleadoActivity.this);

        // Rearmar la empleado
        // Nota: igualmente solamente podríamos mandar el id y recuperar la empleado de la BD
        long idEmpleado = extras.getLong("idEmpleado");
        String nombreEmpleado = extras.getString("nombreEmpleado");
        String fechaNacimientoEmpleado = extras.getString("fechaNacimientoEmpleado");
        String puestoEmpleado = extras.getString("puestoEmpleado");

        empleado = new Empleado(nombreEmpleado, fechaNacimientoEmpleado,puestoEmpleado, idEmpleado);


        // Ahora declaramos las vistas
        etEditarNombre = findViewById(R.id.etEditarNombre);
        etEditarFechaNacimiento = findViewById(R.id.etEditarFechaNacimiento);
        etEditarPuesto = findViewById(R.id.etEditarPuesto);
        btnCancelarEdicion = findViewById(R.id.btnCancelarEdicionEmpleado);
        btnGuardarCambios = findViewById(R.id.btnGuardarCambiosEmpleado);


        // Rellenar los EditText con los datos de la empleado
        etEditarNombre.setText(empleado.getNombre());
        etEditarFechaNacimiento.setText(String.valueOf(empleado.getFecha_nacimiento()));
        etEditarPuesto.setText(empleado.getPuesto());

        // Listener del click del botón para salir, simplemente cierra la actividad
        btnCancelarEdicion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // Listener del click del botón que guarda cambios
        btnGuardarCambios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Remover previos errores si existen
                etEditarNombre.setError(null);
                etEditarFechaNacimiento.setError(null);
                etEditarPuesto.setError(null);
                // Crear la empleado con los nuevos cambios pero ponerle
                // el id de la anterior
                String nuevoNombre = etEditarNombre.getText().toString();
                String posibleNuevaFechaNac = etEditarFechaNacimiento.getText().toString();
                String posibleNuevoPuesto = etEditarPuesto.getText().toString();
                if (nuevoNombre.isEmpty()) {
                    etEditarNombre.setError("Escribe el nombre");
                    etEditarNombre.requestFocus();
                    return;
                }
                if (posibleNuevoPuesto.isEmpty()) {
                    etEditarPuesto.setError("Escribe el puesto");
                    etEditarPuesto.requestFocus();
                    return;
                }
                if (posibleNuevaFechaNac.isEmpty()) {
                    etEditarFechaNacimiento.setError("Escribe la fecha de nacimiento");
                    etEditarFechaNacimiento.requestFocus();
                    return;
                }

                // Si llegamos hasta aquí es porque los datos ya están validados
                Empleado empleadoConNuevosCambios = new Empleado(nuevoNombre, posibleNuevaFechaNac,posibleNuevoPuesto, empleado.getId());
                int filasModificadas = empleadosController.guardarCambios(empleadoConNuevosCambios);
                if (filasModificadas != 1) {
                    // De alguna forma ocurrió un error porque se debió modificar únicamente una fila
                    Toast.makeText(EditarEmpleadoActivity.this, "Error guardando cambios. Intente de nuevo.", Toast.LENGTH_SHORT).show();
                } else {
                    // Si las cosas van bien, volvemos a la principal
                    // cerrando esta actividad
                    finish();
                }
            }
        });
    }
}
