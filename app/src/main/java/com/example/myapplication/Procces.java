package com.example.myapplication;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

public class Procces extends AsyncTask<String,String, Boolean> {
    int segundos = 0;
    OnProccesFinished onProccesFinished;
    ProgressDialog progress;
    public Procces(int _segundos, ProgressDialog _progress, OnProccesFinished _onProccesFinished) {
        segundos = _segundos;
        onProccesFinished = _onProccesFinished;
        progress = _progress;
        Log.d("Inicializa proceso", "Segundos:"+segundos);
    }

    @Override
    protected Boolean doInBackground(String... strings) {
        boolean estatus = true;
        try {
            Log.d("En espera", "Segundos:"+segundos);

            progress.setMax(segundos);
            for(int i=0;i<=segundos;i++){
                progress.setProgress(i);
                Thread.sleep(1000);
            }

            Log.d("finaliza", "Segundos:"+segundos);
            estatus = true;
        } catch (InterruptedException e) {
            estatus = false;
            e.printStackTrace();
        }

        return estatus;
    }

    @Override
    protected void onPostExecute(Boolean estatus) {
        Log.d("onPostExecute", "estatus:"+estatus);
        if(estatus==true){
            onProccesFinished.onFinished("Proceso terminado correctamente!");
        }
        else {
            onProccesFinished.onError("Error en proceso!");
        }
    }

    public interface OnProccesFinished {
        public void onFinished(String mensaje);
        public void onError(String error);
    }
}
