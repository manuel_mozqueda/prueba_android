package com.example.myapplication;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Ejercicio02 extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_ws);
        button = (Button) findViewById(R.id.button);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mMap.clear();
                ConsumerWS servicioWeb = new ConsumerWS("http://74.205.41.248:8081/pruebawebservice/api/mob_sp_GetArchivo");
                JSONObject datosPost = new JSONObject();
                try {
                    datosPost.put("serviceId", 55);
                    datosPost.put("userId", 12984);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                servicioWeb.execute(datosPost);
            }
        });
    }


    private class ConsumerWS extends AsyncTask<JSONObject,Integer,JSONArray> {

        private String url;
        public ConsumerWS(String _url) {
            url = _url;
        }

        protected JSONArray doInBackground(JSONObject... params) {
            Log.d("PARAMETROS", params[0].toString());
            String strJsonRespuesta = "";
            JSONArray jsonArrUbicaciones = null;
            strJsonRespuesta = ejecutarUrl(url,"POST",params[0].toString(),"application/json");
            Log.d("RESPUESTA LOGIN", strJsonRespuesta);
            try {
                JSONArray jsonRespuesta = new JSONArray(strJsonRespuesta);
                String fileDownload = jsonRespuesta.getJSONObject(0).getString("valueResponse");
                if(descargarArchivo(fileDownload,getRutaLocal(),"Ubicaciones.zip") == true){
                    String unzipFile = "";
                    unzipFile = unzip(getRutaLocal()+"/Ubicaciones.zip",getRutaLocal());

                    String strJsonFile = readFile(getRutaLocal()+"/"+unzipFile);
                    JSONObject jsonData = new JSONObject(strJsonFile);

                    jsonArrUbicaciones  = jsonData.getJSONArray("data").getJSONObject(0).getJSONArray("UBICACIONES");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return jsonArrUbicaciones;
        }

        protected void onPostExecute(JSONArray jsonArrUbicaciones) {

            LatLng latLng = null;
            for (int i = 0; i < jsonArrUbicaciones.length(); i++) {
                try {
                    latLng = new LatLng(jsonArrUbicaciones.getJSONObject(i).getInt("FNLATITUD"), jsonArrUbicaciones.getJSONObject(i).getInt("FNLONGITUD"));
                    mMap.addMarker(new MarkerOptions().position(latLng).title(jsonArrUbicaciones.getJSONObject(i).getString("FCNOMBRE")));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            CameraPosition camera = new CameraPosition.Builder()
                    .target(latLng)
                    .zoom(5)           // limit -> 21
                    .bearing(0)         // 0 - 365º
                    .tilt(30)           // limit -> 90
                    .build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(camera));
            //mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        }


    }


    public String ejecutarUrl(String paramURL,String metod,String datos,String contentType){

        try {
            byte[] postDataBytes = datos.toString().getBytes("UTF-8");

            URL url = new URL(paramURL);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod(metod);
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            con.setRequestProperty("Content-Type", contentType);
            con.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
            con.setDoOutput(true);
            con.getOutputStream().write(postDataBytes);
            int responseCode = con.getResponseCode();
            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
                Log.d("Ejecuta url post error", "Server returned HTTP " + con.getResponseCode()+ " " + con.getResponseMessage());
                return "";
            }

            Log.d("Ejecuta url post", "Response code:"+responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            Log.d("Respuesta", ""+response.toString());

            return response.toString();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.d("Ejecuta url get", "Error:"+e.toString());
            return "";
        }

    }



    public boolean descargarArchivo(String varurl,String rutaLocal,String nombreArchivo){
        File file = null;
        try {
            URL url = new URL(varurl);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();

            if (urlConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return false;
            }
            file = new File(rutaLocal,nombreArchivo);

            if (file.exists() ){
                file.delete();
            }

            FileOutputStream fileOutput = new FileOutputStream(file);
            InputStream inputStream = urlConnection.getInputStream();

            byte[] buffer = new byte[1024];
            int bufferLength = 0;
            boolean cancelado = false;
            while ( (bufferLength = inputStream.read(buffer)) > 0 ) {
                fileOutput.write(buffer, 0, bufferLength);
            }

            fileOutput.close();
            inputStream.close();

            if(cancelado == false){
                file = null;
                return true;
            }
            else{
                return false;
            }
            //y gestionamos errores
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }



    public String getRutaLocal(){
        String rutaAbsoluta = "";
        try {
            //creamos el directorio PormoEspacio si no existe
            File sd = new File(Environment.getExternalStorageDirectory(),"Ubicaciones");
            if (!sd.exists()) {
                if (!sd.mkdirs()) {
                    Log.d("getRutaLocal", "Se creo la carpeta Ubicaciones");
                }
            }
            rutaAbsoluta = sd.getAbsolutePath();

        }catch (Exception e) {
            e.toString();
        }

        Log.d("ruta", "ruta:"+rutaAbsoluta);

        return rutaAbsoluta;
    }


    public String unzip(String filePath,String destino) {
        String nameFile = "";
        try {
            FileInputStream inputStream = new FileInputStream(filePath);
            ZipInputStream zipStream = new ZipInputStream(inputStream);
            ZipEntry zEntry = null;

            while ((zEntry = zipStream.getNextEntry()) != null) {
                Log.d("Unzip", "Unzipping " + zEntry.getName() + " at " + destino);

                    FileOutputStream fout = new FileOutputStream(destino + "/" + zEntry.getName());
                    BufferedOutputStream bufout = new BufferedOutputStream(fout);
                    byte[] buffer = new byte[1024];
                    int read = 0;
                    while ((read = zipStream.read(buffer)) != -1) {
                        bufout.write(buffer, 0, read);
                    }

                    zipStream.closeEntry();
                    bufout.close();
                    fout.close();
                    nameFile = zEntry.getName();
            }
            zipStream.close();
            Log.d("Unzip", "Unzipping complete. path :  " + destino);
        } catch (Exception e) {
            Log.d("Unzip", "Unzipping failed");
            e.printStackTrace();
        }

        return nameFile;
    }



    public String readFile(String pathFile){
        String jsonStr = "";
        try {
            File yourFile = new File(pathFile);
            FileInputStream stream = new FileInputStream(yourFile);

            try {
                FileChannel fc = stream.getChannel();
                MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                jsonStr = Charset.defaultCharset().decode(bb).toString();
            }
            catch(Exception e){
                e.printStackTrace();
            }
            finally {
                stream.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonStr;
    }

}
