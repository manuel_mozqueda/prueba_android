package com.example.myapplication;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import android.support.v7.app.AppCompatActivity;

import com.example.myapplication.Procces.OnProccesFinished;

public class Ejercicio03 extends AppCompatActivity implements OnProccesFinished {
    private ProgressDialog progress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio03);

        LinearLayout layout = (LinearLayout) findViewById(R.id.layoutPrincipal);
        Button myboton = new Button(this);
        myboton.setText("Ejecuta proceso");
        myboton.setLayoutParams(new LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT));
        layout.addView(myboton);

        myboton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new Procces(10,progress,Ejercicio03.this).execute();
                progress.show();
            }
        });

        progress = new ProgressDialog(Ejercicio03.this);
        progress.setTitle("Procesando");
        progress.setMessage("Por favor espere ...");
        progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
    }

    @Override
    public void onFinished(String mensaje) {
        Toast.makeText(this,mensaje,Toast.LENGTH_SHORT).show();
        progress.dismiss();
    }

    @Override
    public void onError(String error) {
        Toast.makeText(this,error,Toast.LENGTH_SHORT).show();
        progress.dismiss();
    }
}
