package com.example.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.support.design.widget.FloatingActionButton;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class Ejercicio04 extends AppCompatActivity {
    private List<Empleado> listaDeEmpleados;
    private RecyclerView recyclerView;
    private AdaptadorEmpleados adaptadorEmpleados;
    private EmpleadosController empleadosController;
    private FloatingActionButton fabAgregarEmpleado;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Ojo: este código es generado automáticamente, pone la vista y ya, pero
        // no tiene nada que ver con el código que vamos a escribir
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio04);

        // Lo siguiente sí es nuestro ;)
        // Definir nuestro controlador
        empleadosController = new EmpleadosController(Ejercicio04.this);

        // Instanciar vistas
        recyclerView = findViewById(R.id.recyclerViewEmpleados);
        fabAgregarEmpleado = findViewById(R.id.fabAgregarEmpleado);


        // Por defecto es una lista vacía,
        // se la ponemos al adaptador y configuramos el recyclerView
        listaDeEmpleados = new ArrayList<>();
        adaptadorEmpleados = new AdaptadorEmpleados(listaDeEmpleados);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adaptadorEmpleados);

        // Una vez que ya configuramos el RecyclerView le ponemos los datos de la BD
        refrescarListaDeEmpleados();

        // Listener de los clicks en la lista, o sea el RecyclerView
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override // Un toque sencillo
            public void onClick(View view, int position) {
                // Pasar a la actividad EditarEmpleadoActivity.java
                Empleado empleadoSeleccionada = listaDeEmpleados.get(position);
                Intent intent = new Intent(Ejercicio04.this, EditarEmpleadoActivity.class);
                intent.putExtra("idEmpleado", empleadoSeleccionada.getId());
                intent.putExtra("nombreEmpleado", empleadoSeleccionada.getNombre());
                intent.putExtra("fechaNacimientoEmpleado", empleadoSeleccionada.getFecha_nacimiento());
                intent.putExtra("puestoEmpleado", empleadoSeleccionada.getPuesto());
                startActivity(intent);
            }

            @Override // Un toque largo
            public void onLongClick(View view, int position) {
                final Empleado empleadoParaEliminar = listaDeEmpleados.get(position);
                AlertDialog dialog = new AlertDialog
                        .Builder(Ejercicio04.this)
                        .setPositiveButton("Sí, eliminar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                empleadosController.eliminarEmpleado(empleadoParaEliminar);
                                refrescarListaDeEmpleados();
                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setTitle("Confirmar")
                        .setMessage("¿Eliminar al empleado " + empleadoParaEliminar.getNombre() + "?")
                        .create();
                dialog.show();

            }
        }));


        // Listener del FAB
        fabAgregarEmpleado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Simplemente cambiamos de actividad
                Intent intent = new Intent(Ejercicio04.this, AgregarEmpleadoActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        refrescarListaDeEmpleados();
    }

    public void refrescarListaDeEmpleados() {
        /*
         * ==========
         * Justo aquí obtenemos la lista de la BD
         * y se la ponemos al RecyclerView
         * ============
         *
         * */
        if (adaptadorEmpleados == null) return;
        listaDeEmpleados = empleadosController.obtenerEmpleados();
        adaptadorEmpleados.setListaDeEmpleados(listaDeEmpleados);
        adaptadorEmpleados.notifyDataSetChanged();
    }
}
