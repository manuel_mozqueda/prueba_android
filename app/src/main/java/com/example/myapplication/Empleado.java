package com.example.myapplication;

public class Empleado {

    private String nombre;
    private String fecha_nacimiento;
    private String puesto;

    private long id; // El ID de la BD

    public Empleado(String nombre, String fecha_nacimiento, String puesto) {
        this.nombre = nombre;
        this.fecha_nacimiento = fecha_nacimiento;
        this.puesto = puesto;
    }

    // Constructor para cuando instanciamos desde la BD
    public Empleado(String nombre, String fecha_nacimiento, String puesto, long id) {
        this.nombre = nombre;
        this.fecha_nacimiento = fecha_nacimiento;
        this.puesto = puesto;
        this.id = id;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(String fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Empleado{" +
                "nombre='" + nombre + '\'' +
                ", fecha_nacimiento='" + fecha_nacimiento + '\'' +
                ", puesto='" + puesto + '\'' +
                ", id=" + id +
                '}';
    }
}
