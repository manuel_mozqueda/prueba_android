package com.example.myapplication;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class HelperBD extends SQLiteOpenHelper {
    private static final String NOMBRE_BASE = "empleados_BD", TABLA_EMPLEADOS = "empleados";
    private static final int VERSION_BD = 1;

    public HelperBD(Context context) {
        super(context, NOMBRE_BASE, null, VERSION_BD);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(String.format("CREATE TABLE IF NOT EXISTS %s(id integer primary key autoincrement, nombre text,fecha_nacimiento text, puesto text)", TABLA_EMPLEADOS));
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
