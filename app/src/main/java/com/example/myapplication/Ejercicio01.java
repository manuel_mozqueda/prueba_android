package com.example.myapplication;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class Ejercicio01 extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Button button;
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        button = (Button) findViewById(R.id.button);
        editText = (EditText) findViewById(R.id.editText);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mMap.clear();
                int cantidad = 0;
                if(!editText.getText().toString().equals("")){
                    cantidad = Integer.parseInt(editText.getText().toString());
                }
                else{
                    Toast.makeText(getApplicationContext(), "Escribe un valor", Toast.LENGTH_SHORT).show();
                }

                for(int i=1;i<=cantidad;i++){
                    mMap.addMarker(new MarkerOptions().position(getcCoordenadaAleatoria()).title("Marker No."+i));
                }
            }
        });
    }


    private LatLng getcCoordenadaAleatoria(){
        double minLat = -90.00;
        double maxLat = 90.00;
        double latitude = minLat + (double)(Math.random() * ((maxLat - minLat) + 1));
        double minLon = 0.00;
        double maxLon = 180.00;
        double longitude = minLon + (double)(Math.random() * ((maxLon - minLon) + 1));
        return new LatLng(latitude, longitude);
    }
}
