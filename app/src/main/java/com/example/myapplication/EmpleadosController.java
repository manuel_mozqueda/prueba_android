package com.example.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;


public class EmpleadosController {
    private HelperBD ayudanteBaseDeDatos;
    private String NOMBRE_TABLA = "empleados";

    public EmpleadosController(Context contexto) {
        ayudanteBaseDeDatos = new HelperBD(contexto);
    }


    public int eliminarEmpleado(Empleado empleado) {

        SQLiteDatabase baseDeDatos = ayudanteBaseDeDatos.getWritableDatabase();
        String[] argumentos = {String.valueOf(empleado.getId())};
        return baseDeDatos.delete(NOMBRE_TABLA, "id = ?", argumentos);
    }

    public long nuevompleado(Empleado empleado) {
        // writable porque vamos a insertar
        SQLiteDatabase baseDeDatos = ayudanteBaseDeDatos.getWritableDatabase();
        ContentValues valoresParaInsertar = new ContentValues();
        valoresParaInsertar.put("nombre", empleado.getNombre());
        valoresParaInsertar.put("fecha_nacimiento", empleado.getFecha_nacimiento());
        valoresParaInsertar.put("puesto", empleado.getPuesto());
        return baseDeDatos.insert(NOMBRE_TABLA, null, valoresParaInsertar);
    }

    public int guardarCambios(Empleado empleadoEditado) {
        SQLiteDatabase baseDeDatos = ayudanteBaseDeDatos.getWritableDatabase();
        ContentValues valoresParaActualizar = new ContentValues();
        valoresParaActualizar.put("nombre", empleadoEditado.getNombre());
        valoresParaActualizar.put("nombre", empleadoEditado.getNombre());
        valoresParaActualizar.put("fecha_nacimiento", empleadoEditado.getFecha_nacimiento());
        valoresParaActualizar.put("puesto", empleadoEditado.getPuesto());
        // where id...
        String campoParaActualizar = "id = ?";
        // ... = idEmpleado
        String[] argumentosParaActualizar = {String.valueOf(empleadoEditado.getId())};
        return baseDeDatos.update(NOMBRE_TABLA, valoresParaActualizar, campoParaActualizar, argumentosParaActualizar);
    }

    public ArrayList<Empleado> obtenerEmpleados() {
        ArrayList<Empleado> empleados = new ArrayList<>();
        // readable porque no vamos a modificar, solamente leer
        SQLiteDatabase baseDeDatos = ayudanteBaseDeDatos.getReadableDatabase();
        // SELECT nombre, edad, id
        String[] columnasAConsultar = {"nombre", "fecha_nacimiento", "puesto" , "id"};
        Cursor cursor = baseDeDatos.query(
                NOMBRE_TABLA,//from empleados
                columnasAConsultar,
                null,
                null,
                null,
                null,
                null
        );

        if (cursor == null) {
            /*
                Salimos aquí porque hubo un error, regresar
                lista vacía
             */
            return empleados;

        }
        // Si no hay datos, igualmente regresamos la lista vacía
        if (!cursor.moveToFirst()) return empleados;

        // En caso de que sí haya, iteramos y vamos agregando los
        // datos a la lista de empleados
        do {
            // El 0 es el número de la columna, como seleccionamos
            // nombre, edad,id entonces el nombre es 0, edad 1 e id es 2
            String nombreObtenidoDeBD = cursor.getString(0);
            String fecha_nacimientoDeBD = cursor.getString(1);
            String pueestoDeBD = cursor.getString(2);
            long idEmpleado = cursor.getLong(3);
            Empleado empleadoObtenidaDeBD = new Empleado(nombreObtenidoDeBD, fecha_nacimientoDeBD,pueestoDeBD, idEmpleado);
            empleados.add(empleadoObtenidaDeBD);
        } while (cursor.moveToNext());

        // Fin del ciclo. Cerramos cursor y regresamos la lista de empleados :)
        cursor.close();
        return empleados;
    }
}