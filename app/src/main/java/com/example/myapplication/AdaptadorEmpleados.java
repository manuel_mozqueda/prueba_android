package com.example.myapplication;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import java.util.List;

public class AdaptadorEmpleados extends RecyclerView.Adapter<AdaptadorEmpleados.MyViewHolder> {

    private List<Empleado> listaDeEmpleados;

    public void setListaDeEmpleados(List<Empleado> listaDeEmpleados) {
        this.listaDeEmpleados = listaDeEmpleados;
    }

    public AdaptadorEmpleados(List<Empleado> empleados) {
        this.listaDeEmpleados = empleados;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View filaEmpleado = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fila_empleado, viewGroup, false);
        return new MyViewHolder(filaEmpleado);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        // Obtener la empleado de nuestra lista gracias al índice i
        Empleado empleado = listaDeEmpleados.get(i);

        // Obtener los datos de la lista
        String nombreEmpleado = empleado.getNombre();
        String fechaNacimiento = empleado.getFecha_nacimiento();
        String puesto = empleado.getPuesto();
        // Y poner a los TextView los datos con setText
        myViewHolder.nombre.setText(nombreEmpleado);
        myViewHolder.fechaNacimiento.setText(String.valueOf(fechaNacimiento));
        myViewHolder.puesto.setText(String.valueOf(puesto));
    }

    @Override
    public int getItemCount() {
        return listaDeEmpleados.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView nombre, fechaNacimiento,puesto;

        MyViewHolder(View itemView) {
            super(itemView);
            this.nombre = itemView.findViewById(R.id.tvNombre);
            this.fechaNacimiento = itemView.findViewById(R.id.tvFechaNacimiento);
            this.puesto = itemView.findViewById(R.id.tvPuesto);
        }
    }
}
