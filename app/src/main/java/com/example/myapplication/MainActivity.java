package com.example.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void ejercicio1(View v){
        Intent intent = new Intent(MainActivity.this, Ejercicio01.class);
        startActivity(intent);
    }

    public void ejercicio2(View v){
        Intent intent = new Intent(MainActivity.this, Ejercicio02.class);
        startActivity(intent);
    }

    public void ejercicio3(View v){
        Intent intent = new Intent(MainActivity.this, Ejercicio03.class);
        startActivity(intent);
    }

    public void ejercicio4(View v){
        Intent intent = new Intent(MainActivity.this, Ejercicio04.class);
        startActivity(intent);
    }
}
